#!/bin/bash
export UTILS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PROJECT_ROOT=$UTILS_DIR/../

# Set up colours: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

# create hash map of garden repos - name to repo
declare -A repos=(
    ["schema"]="git@gitlab.com:howlingdingo/schema.git"
)

# create hash map of garden repos - name to repo
declare -A nodeRepos=(
    ["users"]="git@gitlab.com:howlingdingo/users.git"
    ["gardens"]="git@gitlab.com:howlingdingo/gardens.git"
    ["angular-ui"]="git@gitlab.com:howlingdingo/angular-ui.git"
    ["express-gateway"]="git@gitlab.com:howlingdingo/express-gateway.git"
)

cd $PROJECT_ROOT
echo "$green `pwd` $reset"

echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green Checkout out non-node repos $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"

for name in "${!repos[@]}"; do
    echo "$green Looking for: $name -> ${repos[$name]}$reset"
    git clone ${repos[$name]} 2> /dev/null || echo "${yellow}${name} already exists.${reset}" 
done

echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green Checkout out node repos and npm install $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"

for name in "${!nodeRepos[@]}"; do
    echo "$green Looking for: $name -> ${nodeRepos[$name]}$reset"
    git clone ${nodeRepos[$name]} 2> /dev/null || echo "${yellow}${name} already exists.${reset}" 
    cd $name
    npm install
    cd $PROJECT_ROOT
done
cd UTILS_DIR

echo "$green -----$reset"
echo "$green Fin. $reset"
echo "$green -----$reset"
