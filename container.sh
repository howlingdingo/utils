#!/bin/bash

# Set up colours: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

function remove_container {
    if docker ps -a | grep $1
    then
        echo "$yellow $1 - killing existing container $reset"
        docker rm -f $1
    fi
}

# Create a docker network
echo "$red Creating docker network $reset" 
docker network rm gardens-net
docker network create gardens-net
# run psql in a docker container in background
# remove on termination, map container port 5432 to local port 9432
# Set password and container name.

remove_container garden-postgres
echo "$red starting postgres $reset" 
docker run --network gardens-net --network-alias gardens-postgres --name garden-postgres -d --rm -e POSTGRES_PASSWORD=mysecretpassword -p 9432:5432 postgres


export DB_CONNECTION="postgres://postgres:mysecretpassword@gardens-postgres:5432/postgres?sslmode=disable"

echo "Let POSTGRES Start up before running migrations..."
sleep 5

# run the migrations pointing to the docker container
echo "$green Schema $reset"
cd ../schema
remove_container migrations
echo "$yellow Schema - build $reset"
docker build -t garden-migrations .
echo "$red Schema - run $reset"
docker run  -it --name migrations -e GOOSE_DRIVER=postgres -e GOOSE_DBSTRING=$DB_CONNECTION --network gardens-net garden-migrations

#
# Build and run the services
#
echo "$green Users $reset"
cd ../users/
remove_container users
echo "$yellow Users - build $reset"
docker build -t andy/howling-dingo-users .
echo "$red Users - run $reset"
docker run -d \
 -p 43010:3010 \
 --network-alias users \
 --name users \
 --network gardens-net \
 -e DB_CONNECTION=postgres://postgres:mysecretpassword@gardens-postgres:5432/postgres?sslmode=disable \
 -e PORT=3010 \
 andy/howling-dingo-users



echo "$green Gardens $reset"
cd ../gardens/
remove_container gardens
echo "$yellow Gardens - build $reset"
docker build -t andy/howling-dingo-gardens .
echo "$red Gardens - run $reset"
docker run -d \
 -p 43030:3030 \
 --network gardens-net \
 --network-alias gardens \
 --name gardens \
 -e DB_CONNECTION=postgres://postgres:mysecretpassword@gardens-postgres:5432/postgres?sslmode=disable \
 -e PORT=3030 \
 andy/howling-dingo-gardens

echo "$green Angular $reset"
cd ../angular-ui/

echo "$yellow Angular - build $reset"
remove_container angular-ui
docker build -t andy/howling-dingo-angular-ui .
echo "$red Angular - run $reset"
docker run -d \
 -p 44200:80 \
 --network gardens-net \
 --network-alias angular-ui \
 --name angular-ui \
  andy/howling-dingo-angular-ui

### Finally, set up the node based gateway to route the requests to the containers! 
cd ../express-gateway

echo "$yellow Express gateway - build $reset"
remove_container express-gateway
docker build -t andy/howling-dingo-express-gateway .
echo "$red Express gateway - run $reset"
docker run -d \
 -p 48080:8080 \
 -e DOCKER=1 \
 --network gardens-net \
 --network-alias express-gateway \
 --name express-gateway \
  andy/howling-dingo-express-gateway

