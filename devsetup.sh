#!/bin/bash

# Set up colours: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

if [ "root" = `whoami` ]; then
	echo "$red ---------------------------------------------- $reset"
	echo "$red ---------------------------------------------- $reset"
	echo "$red Please run as normal user with sudo privileges $reset"
	echo "$red ---------------------------------------------- $reset"
	echo "$red ---------------------------------------------- $reset"
	exit 1
fi

### TODO: Add switches to skip some steps if required...

echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green Setting up development environment $reset"
echo "$green ---------------------------------------------- $reset"
echo "$green ---------------------------------------------- $reset"


echo "$green Doing apt update / upgrade $reset"
sudo apt update
sudo apt -y upgrade


echo "$green Install utils: curl and jq (json thing) $reset"
sudo apt-get -y install curl jq

echo "$green install node and npm (node package manager) $reset"
echo "$green Get the deb package from nodesource.com - 14 is current latest LTS $reset"
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get -y install nodejs

echo "$green Fix permisssion issues with npm install global... $reset"
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo "export PATH=~/.npm-global/bin:$PATH" >> ~/.profile
source ~/.profile

echo "$green Install @angular $reset"
npm install -g @angular/cli

echo "$green Install postgres $reset"
sudo apt-get -y install postgresql

echo "$green Create user in local database, use local user as user and pw. Give all privs and create database $reset"
sudo -u postgres psql -c "CREATE USER `whoami` PASSWORD '`whoami`';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO `whoami`;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO `whoami`;"
sudo -u postgres psql -c "ALTER USER `whoami` WITH SUPERUSER;"
sudo -u postgres createdb gardens

echo "$green Install docker and set up group for current user $reset"
sudo apt-get install docker.io
sudo usermod -aG docker $USER
newgrp docker 

echo "$green We use Goose to for database schema migrations: github.com/pressly/goose/cmd/goose $reset"
echo "$green Install Go - Use 1.18 -> https://go.dev/doc/install $reset"
curl -L -o go1.18.linux-amd64.tar.gz https://go.dev/dl/go1.18.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.18.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.profile
source ~/.profile

echo "$green Install Goose $reset"
go install github.com/pressly/goose/v3/cmd/goose@latest

echo "$green Fin! $reset"
exit 0

