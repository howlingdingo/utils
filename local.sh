# Set up colours: https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0`

# Get current working dir: https://stackoverflow.com/questions/59895/how-can-i-get-the-source-directory-of-a-bash-script-from-within-the-script-itsel
export UTILS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PROJECT_ROOT=$UTILS_DIR/../
export DB_CONNECTION="postgres://`whoami`:`whoami`@localhost:5432/gardens?sslmode=disable"

#!/bin/bash
if ! [[ -x "$(command -v pm2)" ]]; then
    echo "PM2 is not installed. Installing..."
    sudo npm install pm2@latest -g
fi

# assumes goose installed as per devsetup.sh
echo "SCHEMA UPDATE"
cd ../schema
~/go/bin/goose -dir db/migrations/  postgres "user=`whoami` password=`whoami` dbname=gardens sslmode=disable"  up
cd ../utils

echo "Starting Node apps"
pm2 kill
pm2 start pm2.config.js
echo "Letting angular start up (it is slow....)"
sleep 3
pm2 ps
echo
echo
echo "$green To interact with locally running app through gateway, navigate to: $reset"
echo "$cyan http:localhost:8080$reset"
echo
echo "$green To connect directly to a service:$reset"
echo "$cyan Angular ui --> localhost:4200$reset"
echo "$cyan Services   --> localhost:30x0$reset"
echo
echo "$green For process status:$reset"
echo "$cyan pm2 ps$reset"
echo
echo "$green For process logs:$reset"
echo "$cyan pm2 logs$reset"
echo "$cyan pm2 logs <SERVICE_NAME>$reset"
echo
echo "$green To stop everything:$reset"
echo "$cyan pm2 kill$reset"
echo