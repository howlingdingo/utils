#!/bin/bash

# run psql in a docker container in background
# remove on termination, map container port 5432 to local port 9432
# Set password and container name.
docker run --name garden-postgres -d --rm -e POSTGRES_PASSWORD=mysecretpassword -p 9432:5432 postgres
export DB_CONNECTION="postgres://postgres:mysecretpassword@localhost:9432/postgres?sslmode=disable"

cd ../schema
sudo docker build -t . -t limivourous:32000/garden-migrations:v1
# sudo docker run -it --rm --network host -e GOOSE_DRIVER=postgres -e GOOSE_DBSTRING=$DB_CONNECTION garden-migrations


cd ../user/
docker build . -t andy/howling-dingo-users
# docker run -p 43010:3010 -d --name users --add-host dockerhost:172.17.0.1 andy/howling-dingo-users

cd ../gardens/
docker build . -t andy/howling-dingo-gardens
# docker run -p 43030:3030 -d --name gardens --add-host dockerhost:172.17.0.1 andy/howling-dingo-gardens

cd ../angular-ui/
docker build . -t andy/howling-dingo-angular-ui
# docker run -p 44200:42000 -d --name angular-ui andy/howling-dingo-angular-ui