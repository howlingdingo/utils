"use strict";

const path = require("path");
const homedir = require("os").homedir();

const PROJECT_ROOT = process.env["PROJECT_ROOT"];
const DB_CONNECTION = process.env["DB_CONNECTION"];

module.exports = {
  apps: [
    {
      name: "gateway",
      script: "npm",
      args: "start",
      cwd: path.join(PROJECT_ROOT, "express-gateway"),
      watch: true,
      instance_var: "INSTANCE_ID",
      env: {
      }
    },

    {
      name: "frontend",
      script: "npm",
      args: "start",
      cwd: path.join(PROJECT_ROOT, "angular-ui"),
      watch: false,
      env: {
      }
    },
    {
      name: "users",
      script: "npm",
      args: "start",
      cwd: path.join(PROJECT_ROOT, "users"),
      watch: true,
      instance_var: "INSTANCE_ID",
      env: {
        DB_CONNECTION: DB_CONNECTION,
        PORT: 3010,
      }
    },
    {
      name: "gardens",
      script: "npm",
      args: "start",
      cwd: path.join(PROJECT_ROOT, "gardens"),
      instance_var: "INSTANCE_ID",
      watch: true,
      env: {
        DB_CONNECTION: DB_CONNECTION,
        PORT: 3030,
      }
    },
  ]
};
