#!/bin/bash
#cd ..
#find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && git status -s -b && echo)' \;

# for dir in ../*
# do
#     cd dir
#     git status -s -b
#     echo
#     cd -
# done


for dir in ../*
do
    echo $dir
    cd $dir
    [ -d "./.git" ] && git status -s -b || echo "No git repo"
    echo
done
